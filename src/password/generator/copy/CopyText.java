package password.generator.copy;

import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;


/**
 * Class CopyText
 *
 * The CopyText class has the getCopyText method
 *
 */

public class CopyText {

    /**
     * The getCopyText method performs the copy button action
     *
     * @param textCopied Get the generated password from the text box
     */
     public static void getCopytext(JTextField textCopied){

         StringSelection stringSelection = new StringSelection(textCopied.getText());
         Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
         clipboard.setContents(stringSelection,null);

     }

}
