package password.generator.condition;

import password.generator.generators.ServiceGenerator;

import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

public class JcheckBoxConditions{

     private static boolean condition;
     private final static  String str = "";
     private final static ServiceGenerator serviceGenerator = new ServiceGenerator();

     public static boolean isFalseCondition(JCheckBox jCheckBoxFirst , JCheckBox jCheckBoxSecond , JCheckBox jCheckBoxThird, JCheckBox jCheckBoxFourth){
     return condition = !jCheckBoxFirst.isSelected() && !jCheckBoxSecond.isSelected() && !jCheckBoxThird.isSelected() && !jCheckBoxFourth.isSelected();
     }

     public  static  boolean isTrueCondition(JCheckBox jCheckBoxFirst , JCheckBox jCheckBoxSecond , JCheckBox jCheckBoxThird, JCheckBox jCheckBoxFourth){
          return condition = jCheckBoxFirst.isSelected() && jCheckBoxSecond.isSelected() && jCheckBoxThird.isSelected() && jCheckBoxFourth.isSelected();
     }


     public static boolean isTrueTwoConditions(JCheckBox jCheckBoxFirst , JCheckBox jCheckBoxSecond){
          return condition = jCheckBoxFirst.isSelected() && jCheckBoxSecond.isSelected();
     }

     public static void actionJcheckBox(JCheckBox jCheckBoxFirst , JCheckBox jCheckBoxSecond , JCheckBox jCheckBoxThird, JCheckBox jCheckBoxFourth, JTextField texto){


          if (isFalseCondition(jCheckBoxFirst,jCheckBoxSecond,jCheckBoxThird,jCheckBoxFourth)) {
               JOptionPane.showMessageDialog(null, "MARQUE UMA CAIXA","BOXES IS NOT SELECTED" , JOptionPane.WARNING_MESSAGE);

          } else if(isTrueCondition(jCheckBoxFirst,jCheckBoxSecond,jCheckBoxThird,jCheckBoxFourth)){
               JOptionPane.showMessageDialog(null, "MARQUE UMA CAIXA DE CADA VEZ","ALL BOXES HAVE BEEN CHECKED", JOptionPane.WARNING_MESSAGE);

          }else if(isTrueTwoConditions(jCheckBoxFirst,jCheckBoxSecond) || isTrueTwoConditions(jCheckBoxThird , jCheckBoxFourth) ||
                   isTrueTwoConditions(jCheckBoxFirst,jCheckBoxThird)  || isTrueTwoConditions(jCheckBoxFirst,jCheckBoxFourth) ||
                   isTrueTwoConditions(jCheckBoxSecond, jCheckBoxThird)|| isTrueTwoConditions(jCheckBoxSecond,jCheckBoxFourth)) {
               JOptionPane.showMessageDialog(null, "MARQUE UMA CAIXA" ,"TWO BOXES SELECTED" , JOptionPane.WARNING_MESSAGE );

          }else {
               if (jCheckBoxFirst.isSelected()) {
                    texto.setText(serviceGenerator.generator(str));
               }

               if (jCheckBoxSecond.isSelected()) {
                    texto.setText(serviceGenerator.numbers(str));
               }

               if (jCheckBoxThird.isSelected()) {
                    texto.setText(serviceGenerator.UpperCase(str));
               }

               if (jCheckBoxFourth.isSelected()) {
                    texto.setText(serviceGenerator.lowerCase(str));
               }
          }
     }
}
