package password.generator.condition;

import password.generator.copy.CopyText;
import password.generator.savepasswords.SavePasswords;

import javax.swing.*;

public class JtextConditions {


    public static void isJtextEmpty(JTextField jTextField){

        if(jTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "CAIXA DE TEXTO ESTA VAZIA", "EMPTY TEXT", JOptionPane.WARNING_MESSAGE);
        }else{
            SavePasswords.createFileAndDirs(jTextField);
       }
    }

    public static void isJtextCopyEmpty(JTextField jTextField){

        if(jTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "CAIXA DE TEXTO ESTA VAZIA NA HA NADA PARA SER COPIADO", "EMPTY TEXT", JOptionPane.WARNING_MESSAGE);
        }else{
            CopyText.getCopytext(jTextField);
        }
    }
}
