package password.generator.screen;

import password.generator.condition.JcheckBoxConditions;
import password.generator.condition.JtextConditions;
import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * ScreenGenerator class
 *
 * This is the class that creates the graphical interface of the application
 *
 * @version 1.0
 *
 */

public class ScreenGenerator extends JFrame {

     /*buttons*/
     private final JButton generate, clear, save, copy;

     /*panel*/
     private final JPanel panel;

     /*text box*/
     private final JTextField text;

     /*labels*/
     private final JLabel pass, Char1, Char2, Char3, Char4;

     /*selection boxes*/
     private final JCheckBox selectOne, selectTwo, selectThree, selectFour;

     /*image icon for copy button*/
     private final ImageIcon icon;



/**
 *
 * Constructor method
 *
 */
   public ScreenGenerator() {
        super("GERADOR DE SENHAS");

        /* frame  configuration */
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(395, 190);
        setResizable(false);


        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2, 2, 2, 2);

        panel = new JPanel(new GridBagLayout());
        panel.setBackground(Color.WHITE);

        add(panel);

        icon = new ImageIcon(Objects.requireNonNull(this.getClass().getClassLoader().getResource("resource/icon-copy.png")));
        copy = new JButton();
        generate = new JButton("GERAR");
        clear = new JButton("LIMPAR");
        save = new JButton("Salvar-Senha");

        text = new JTextField();
        text.requestFocus(true);
        gbc.gridx = 1;
        gbc.gridy = 0;
        text.setEditable(false);
        text.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        text.setBackground(Color.WHITE);
        gbc.fill = GridBagConstraints.HORIZONTAL;

        panel.add(text, gbc);

        pass = new JLabel("  SENHA: ");
        pass.setFont(new Font("Courier", Font.PLAIN + Font.ITALIC, 15));
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(pass, gbc);

        Char1 = new JLabel("A,0,%,a:");
        gbc.gridx = -0;
        gbc.gridy = 8;
        panel.add(Char1, gbc);


        Char2 = new JLabel("0a10");
        gbc.gridx = -1;
        gbc.gridy = 8;
        panel.add(Char2, gbc);

        Char3 = new JLabel("A,A,A:");
        gbc.gridx = -1;
        gbc.gridy = 8;
        panel.add(Char3, gbc);

        Char4 = new JLabel("a,a,a:");
        gbc.gridx = -1;
        gbc.gridy = 8;
        panel.add(Char4, gbc);

        selectOne = new JCheckBox();
        gbc.gridx = -1;
        gbc.gridy = 9;
        selectOne.setBackground(Color.WHITE);
        panel.add(selectOne, gbc);


        selectTwo = new JCheckBox();
        gbc.gridx = -1;
        gbc.gridy = 9;
        selectTwo.setBackground(Color.WHITE);
        panel.add(selectTwo, gbc);

        selectThree = new JCheckBox();
        gbc.gridx = -1;
        gbc.gridy = 9;
        selectThree.setBackground(Color.WHITE);
        panel.add(selectThree, gbc);

        selectFour = new JCheckBox();
        gbc.gridx = -1;
        gbc.gridy = 9;
        selectFour.setBackground(Color.WHITE);
        panel.add(selectFour, gbc);

        copy.addActionListener(e -> {

             JtextConditions.isJtextCopyEmpty(text);
             copy.setFocusable(false);
             copy.setFocusable(true);
             copy.setForeground(Color.BLACK);

        });

        gbc.gridx = 2;
        gbc.gridy = 0;
        copy.setIcon(icon);
        copy.setBackground(Color.WHITE);
        copy.setBorder(BorderFactory.createEmptyBorder());
        copy.setPreferredSize(new Dimension(0,20));
        copy.setContentAreaFilled(false);
        panel.add(copy,gbc);



        generate.addActionListener(e ->{
             text.setBorder(BorderFactory.createEmptyBorder());
             JcheckBoxConditions.actionJcheckBox(selectOne, selectTwo, selectThree, selectFour, text);

        });
        gbc.gridx = 0;
        gbc.gridy = 2;
        generate.setBackground(Color.WHITE);
        generate.setFocusable(false);
        generate.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(generate, gbc);

        clear.addActionListener(e -> {
             text.setBorder(BorderFactory.createLineBorder(Color.BLACK));
             text.setText(null);
        });

        gbc.gridx = 2;
        gbc.gridy = 2;
        clear.setBackground(Color.WHITE);
        clear.setFocusable(false);
        clear.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(clear, gbc);

        save.addActionListener(e -> JtextConditions.isJtextEmpty((text)));

        gbc.gridx = 1;
        gbc.gridy = 2;
        save.setBackground(Color.white);
        save.setFocusable(false);
        save.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(save, gbc);

    }
}
