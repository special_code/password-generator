package password.generator.generators;

import password.generator.generators.Generators;
import password.generator.generators.LogicGenerator;

public class ServiceGenerator extends Generators<LogicGenerator> {


    public ServiceGenerator() {
        super(LogicGenerator.class);
     }

     public String generator(String str){
        return allMethod(str);
     }

     public String numbers(String str){
        return allnumber(str);
    }

     public String UpperCase(String str){
        return  allUpperCase(str);
    }

     public String lowerCase(String str){
        return allLowerCase(str);
    }


}
