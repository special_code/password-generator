package password.generator.generators;

import java.util.Random;

public class LogicGenerator {

   private final Random random = new Random(System.currentTimeMillis());

    private final char[] isAllChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'x', 'z', 'w', 'y', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '/', '|',
            '%', '?', '*', '+', ' ', '#', '@', '^', '~', '_',',','?', '-',
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Z', 'W', 'Y'};


    private final String[] alphabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "x", "z", "w", "y"};

    public String generator(String numGenerator) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i <= 11; i++) {
            int ch = random.nextInt(isAllChars.length);
            stringBuffer.append(isAllChars[ch]);

        }

        numGenerator = stringBuffer.toString();


        return numGenerator; //stringBuffer.toString();

    }

    public String generatorNumbers(String c) {
        for (int i = 20; i <= 50; i++) {
            i = random.nextInt();
            c = i + "";
        }
        return c;

    }

    public String generatorUppperCase(String generator) {
        StringBuffer bs = new StringBuffer();

        for (int i = 0; i < 10; i++) {
            int a = random.nextInt(alphabet.length);
            bs.append(alphabet[a]);
            generator = bs.toString();
        }
        return generator.toUpperCase();

    }

    public String generatorLowerCase(String generator) {
        StringBuffer bs = new StringBuffer();

        for (int i = 0; i < 10; i++) {
            int a = random.nextInt(alphabet.length);
            bs.append(alphabet[a]);
            generator = bs.toString();
        }
        return generator.toLowerCase();

    }


}
