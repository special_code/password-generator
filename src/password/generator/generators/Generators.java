package password.generator.generators;

public class Generators<T> {
    protected Class<T>tClass;
    protected LogicGenerator logicGenerator;

    public Generators(Class tClass){
        this.tClass = tClass;
    }

    public LogicGenerator getAllGeracoes() {

        if(logicGenerator != null){
            return logicGenerator;
        }else if(logicGenerator ==  null) {
            logicGenerator = new LogicGenerator();
            return logicGenerator;
       }

        return null;
    }

    public void setAllGeracoes(LogicGenerator allGeracoes) {
        this.logicGenerator = allGeracoes;
    }

    public  String allMethod(String strAll) {
        return getAllGeracoes().generator(strAll);
    }

    public  String allnumber(String strNumbers) {
      return getAllGeracoes().generatorNumbers(strNumbers);
    }

    public String allUpperCase(String strAllCharactersUp) {
        return getAllGeracoes().generatorUppperCase(strAllCharactersUp);
    }

    public String allLowerCase(String strAllCharacterTiny) {
        return getAllGeracoes().generatorLowerCase(strAllCharacterTiny);
    }
}
