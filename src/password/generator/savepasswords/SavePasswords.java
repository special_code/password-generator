package password.generator.savepasswords;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;


/**
 * SavePasswords class
 *
 * This class saves the generated passwords in a specific file created by the application itself
 *
 * It stores passwords in plain text. There is not even a type of encryption implementation.
 *
 * @version 1.0
 *
 */
public class SavePasswords {

       private static final String URL_DIR = System.getProperty("user.home");// creates the directory in the user's home folder
       private static final String fileName = "bkp.BACKUP"; //file name
       private static final String nameDirectry = "/.bkp-geradordesenha"; // directory name

       /**
        *
        * method createFileAndDirs creates a file and a folder, file to record the
        * passwords
        * @return File return a file
        * @param jTextField get the text that is generated to be saved in the file
        */
       public static File createFileAndDirs(JTextField jTextField){

           OffsetDateTime offsetDateTime = OffsetDateTime.now();
           DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy : HH:mm:ss");


           //createDir
           var directory = new File(URL_DIR+nameDirectry);

           if(!directory.exists()) {
               directory.mkdir();
           }


           //createFile
           var file = new File(directory,fileName);


           try(var fileWriter = new FileWriter(file, true)) {

               var bufferedWriter = new BufferedWriter(fileWriter);

               JOptionPane.showMessageDialog(null, "Guardando senha em: " + file.getAbsolutePath() + "\nSenha: " + jTextField.getText(),"STORE PASSWORD" ,JOptionPane.INFORMATION_MESSAGE);

               bufferedWriter.write(jTextField.getText() +":"+ dateTimeFormatter.format(offsetDateTime));
               bufferedWriter.newLine();
               bufferedWriter.close();

           } catch (IOException e) {
              JOptionPane.showMessageDialog(null,"ERROR"+e.getMessage() , "BAD FORMAT FILE",JOptionPane.WARNING_MESSAGE);
           }

           return file;
       }
}

