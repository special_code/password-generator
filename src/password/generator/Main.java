package password.generator;

import password.generator.screen.ScreenGenerator;

import javax.swing.*;

/**
 *  Simple application to generate random passwords
 *
 *  This class has the main method, creates the anonymous object of the ScreenGenerator class and opens the interface for
 *  the user to interact with the application
 *
 *
 *  This application was created for didactic purposes and practices
 *
 *  @version 1.0
 *  @author fhelliphe
 */
public class Main {
     /**
      *  This is method main
      *  creates the anonymous object of the ScreenGenerator
      *  class and opens the interface for the user to interact
      *  with the application
      *  @param args unused command line
      */

public static void main(String[] args){

    // creates a ScreenGenerator object
    SwingUtilities.invokeLater(() -> new ScreenGenerator().setVisible(true));

    }
}